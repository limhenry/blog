---
layout: post
title:  "My First Post"
date:   2015-01-30 18:17:00
---

Hello World.

I am Henry Lim.

I am a student, a developer and a human of course.

This is my first blog and it's hosted on GitHub. Anyway, this awesome blog is built by using [Jekyll]. It's quite simple to build a blog like this. In the meantime, this blog is still under development and the blog (layout) will be updated from time to time.

Sometimes, you might ask me, why don’t I use blogger or wordpress? In Blogger/WordPress, you just write your post, and you can straight away post it.

But in GitHub, you need to create a blog by yourself (thanks to Jekyll, that saved my life…). Then, you can change the layout by using CSS without any limitation (which is nearly impossible in Blogger/WordPress), and I had a very lousy reason to learn HTML/CSS ( ͡° ͜ʖ ͡°), LOL.

I love web, so I love HTML. Of course, I only love a beautiful website, maybe Material Design?

Since this blog is still under development, it’s time to let me continue to make this website better and nicer,

Thank you for reading my first post on this website.

By the way, remember to visit my home page : [limhenry.github.io]


Have a nice day ^^


[Jekyll]:	http://jekyllrb.com/
[limhenry.github.io]:	http://limhenry.github.io