---
layout: post
title:  "Quick review of my PC (Ubuntu)"
date:   2015-01-30 21:15:00
---

A quick review of my PC with Ubuntu

I am using ASUS X550LD right now, overall it is OK. This computer is pre-installed with Windows 8.1 but...... (Most of the time) I am using Ubuntu 14.04.1. 

Why? Is Ubuntu better than Windows?

The answer is yes and no. I can't afford to buy a MacBook, so I 'force' to buy a Windows PC. And now, I installed this Ubuntu to my lappy. Firstly, it runs sucks because the graphic card (NVIDIA Geforce 820M) can't detect, until one day (I also dunno the reason) my Ubuntu detected that graphic card without any reason. 

Ok, now graphic card is detected, it is time to say goodbye to Windows. 

For me, Ubuntu is an OS like Mac OS, but you "can't run Photoshop". It's fast, just I hate that default design. Thanks to [Hii](https://github.com/HiiYL), he introduced and taught me something very awesome which is called "Gnome". It's awesome and beautiful. Somehow I love [Material Design](http://www.google.com/design/spec/material-design/introduction.html) so much, so I decided to make my Ubuntu more material design. In conclusion, it's very very nice!

Performance? Yes, it is better than Windows. For example, it's easier to run Jekyll in Ubuntu compare to Windows. Another example is: Android Studio.

Last time, I didn't know Ubuntu is so fast, then I using Windows to develop and build some Android app. It's suck, slow and you will see "Not Responding" almost everyday (every hour, maybe?). The compile app duration in Windows is around 3-4 minutes (in my case), but when I compile in Ubuntu, it only took 30-40 seconds. Seriously? Yep, Ubuntu is amazing.

One thing (two actually), make my Ubuntu a bit 'haiz' are speaker and touchpad. Due to kernel/driver problem, that touchpad doesn't work with my laptop, luckily I got mouse. Next, speaker. The sound in Ubuntu is really very 'unlistenable'. Luckily I got [Pulse Audio Equalizer](http://www.webupd8.org/2013/03/install-pulseaudio-with-built-in-system.html), which make my laptop's speaker better a little bit, but not so nice. 

How about the Microsoft latest OS, Windows 10, which is still under technical preview, it's still very buggy. For me, Windows 7, 8, 8.1 and 10 are same. But what I need is a fast OS, so Hello Ubuntu, Hello Linux, Hello World.

I will talk about my opinion and hand-on with Windows 10 Technical Preview in next post. Thanks for reading my post, have a nice day. ^^


Scereenshot:

{% include image.html url="http://i.imgur.com/ubCiK7d.png" %} 
{% include image.html url="http://i.imgur.com/taVGc9q.png" %} 
{% include image.html url="http://i.imgur.com/hJh5gtG.png" %} 
{% include image.html url="http://i.imgur.com/JVZmfQy.png" %} 
{% include image.html url="http://i.imgur.com/7iaafQq.png" %} 
{% include image.html url="http://i.imgur.com/txYvGGz.png" %} 
{% include image.html url="http://i.imgur.com/E5ddFzw.png" %} 