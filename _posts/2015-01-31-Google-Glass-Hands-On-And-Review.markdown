---
layout: post
title:  "Google Glass, Hands On and a very quick review"
date:   2015-01-31 14:20:00
---

Google Glass, Google Glass, Google Glass, Google Glass, Google Glass, Google Glass, Google Glass, Google Glass...

It became my favourite tech gadget the moment it announced to public.

Anyway, I managed to borrow that awesome Google Glass from [Yap Wen Jiun](http://www.wenjiun.my/). 

Google Glass is awesome, it’s light and it’s amazing.

You probably know how it works and how it look like. But it is hard to know the feeling when you are wearing it.

So, I got the chance to borrow it after the MMU Android Meetup in 31 October 2014. I was so excited when that Google Glass is really in front of me. I took out my glasses, and put the Google Glass on my face. Something happens! I saw a blur screen in front of me. Kay, I immediately knew the reason and finally, I ‘force’ to wear double glasses (my perspective glasses + Google Glass).

Double glasses on my face. It’s weird. But whatever, just ignore it.

The speaker of Google Glass. It’s awesome. It uses the Bone Conduction Transducer (BCT), so the sound is pretty amazing, it’s loud and crystal clear. Microphone? Awesome. I was using that Google Glass in a hall (quite noisy there), but Google Glass was still listening to what I am talking about.

Screen? Amazing. The HUD makes you like Iron Man, a small rectangle screen floating in front of you (it is around 2-3m in front of you). And it’s ‘floating’. That is quite a weird experience. And it’s also looks semi-transparent, so the screen will not bother you. 

Ok Glass, take a picture. Ok Glass, record a video. Ok Glass, Google, what is Google Glass? I was so busy with trying that piece of technology. That is the real technology. The future is here. 

Google Glass is expensive. It costs you USD1500 or it’s around MYR5000. IT IS VERY VERY EXPENSIVE! I am just waiting for one day (and I believe it’s coming), a cheaper version of Google Glass is going to release to the consumer, or maybe less than USD300? 

It’s coming soon to us because [“Google Glass is graduating from Google[x] labs” ](https://plus.google.com/111626127367496192147/posts/9uiwXY42tvc). I hope Google Glass Consumer Edition will announce during Google I/O this year, maybe?

Hello Glass, Hello Google, Hello World.

{% include image.html url="http://i.imgur.com/W793fXo.jpg" description="Google Glass" %} 
{% include image.html url="http://i.imgur.com/UoqkTtw.jpg" description="Google Glass" %} 
{% include image.html url="http://i.imgur.com/xxlsjlz.jpg" description="Google Glass" %} 
{% include image.html url="http://i.imgur.com/SV8V5Lf.jpg" description="Google Glass" %} 





