---
layout: post
title:  "Windows 10 Review"
date:   2015-02-04 12:00:00
---

Microsoft announced the latest build of Windows 10 Technical Preview Build 9926 on 23 January 2015.

Windows 10. Pricing? Free, seriously. Windows 10 will be available to be upgraded for every Windows 7, 8 and 8.1 users. FREE. No hidden charge. Just you are not running a pirated version of Windows, LOL.

So, are there any new things/features in this latest build of Windows 10?


##Hey Cortana.
First, CORTANA. People love technology. People love artificial intelligence (AI). There are Siri for iOS users, Google Now for Android (and iOS, oops) and Cortana for Windows phone. 

Now, the biggest problem is: computer should be smarter than smartphone, every features in smartphone should also be available in computer. So, Microsoft introduces Cortana, a Microsoft AI for Windows 10.

Is that awesome? Currently, no. Maybe it’s still under development/beta. Anyway, how is the quality/performance/feature of Cortana in this latest build of Windows 10? 

The available features are still very limited. You only can set reminders, check weather forcast, and I think that’s all. You can’t ask her (Cortana is a female…) “who is Obama?”. If you ask her, she will simply redirect you to Bing.com (Where is my Google?)

So, Cortana in Windows 10, let’s ignore this feature until the next build or the final release of Windows. Hope Cortana will become a better and awesome future.


##Hello Start Menu.
Next, Start Menu is back! But don’t be so happy. The ‘new’ start menu is something like the combination of the start menu of Windows 7 and metro start of Windows 8. It looks weird and I don’t like it. I still prefer Windows 8 metro start or Windows 7 start menu. Just don’t combine together. Anyway, it should work better in touch screen mode.


##Virtual Desktop
Virtual Desktop. This is one of the features in Mac OS long long time ago. And finally it will be available in Windows (without any third-party add-ons). It works quite nicely but it’s a little bit unfamiliar to Windows users. It’s still a nice feature anyway.


##A Windows PC for just only USD 35?
Wait wait. Did Microsoft say Windows 10 is an OS for any devices from no screen to phone, tablet, laptop, PC, even TV? Yes. Windows 10 is bigger and stronger than you think.

A Windows PC for just 35USD? Seriously, Windows JUST announced that Windows 10 will also be available for Raspberry Pi 2 for free. Our next generation, 35USD computer is coming soon. More info: [raspberrypi.org](http://www.raspberrypi.org/raspberry-pi-2-on-sale/)

Windows 10 is nice. But I will continue using my beautiful and fast Ubuntu i mentioned few days ago. [Link](http://limhenry.github.io/blog/2015/01/30/Quick-review-of-My-PC.html)

Thanks for reading my blog and supporting me, have a nice day.

{% include image.html url="http://i.imgur.com/XGte9Y7.png" %}
{% include image.html url="http://i.imgur.com/Aq8f9dk.png" %}
{% include image.html url="http://i.imgur.com/ZpPqiTf.png" %}
{% include image.html url="http://i.imgur.com/MNISvh0.png" %}
{% include image.html url="http://i.imgur.com/ReJCwO3.png" %}
{% include image.html url="http://i.imgur.com/hNUwPTU.png" %}






