---
layout: post
title:  "My Favorite Android Apps"
date:   2015-02-08 12:00:00
---

I am using Android phone right now, because it is nice and good. But not just the hardware make my Android phone nice but apps.

Today I am going to introduce some of my favorite apps.

#Google Now Launcher

Google Now Launcher is developed by Google. It is useful if you are Google Now daily user (hourly?) because this launcher allows user to ‘swipe right to Google Now’. This allows me to access to Google Now even faster than ‘swipe up to Google Now’. This is definitely my favorite launcher. It’s fast and beautiful. 

By the way,the latest version of Google Now Launcher have the Material Design (but no that awesome animation) for Android 4.1 - Android 4.4. Of course, Android 5.0/5.1 will include that animation (ripple effect).

Why I am not using other launcher like Nova, Apex or stock (default) launcher? This Google Now Launcher is the only launcher makes my phone looks like stock Android. I prefer clean design, and Google Now Launcher is what I need.

<div class="pb-app-box" data-theme="discover" data-lang="en"><a href="http://playboard.me/android/apps/com.google.android.launcher">Google Now Launcher  (Playboard)</a> | <a href="https://play.google.com/store/apps/details?id=com.google.android.launcher&hl=en" rel="nofollow" target="_blank">Google Now Launcher (Play Store)</a></div>
<script type="text/javascript" src="//playboard.me/widgets/pb-app-box/1/pb_load_app_box.js"></script><br>


#OfficeSuite 8#

This app is like a mobile version of Microsoft Office but it for Android phone/tablet. It has almost all the features you need. You can do almost everything in Office Suite 8. You can create and view .docx, .xlsx., .pptx, etc. files with this app. You also can insert image into your document file (this is impossible to do it in [Google Docs](https://play.google.com/store/apps/details?id=com.google.android.apps.docs.editors.docs). 

Actually, I using the Office Suite Pro 8 right now, so I can access to all their awesome features. Anyway, I didn’t brought it (not pirated, thanks) but I get it for FREE from Amazon App Store (sorry, that promotion ended long long time ago). 

Anyway, no matter it’s free version of paid (pro) version, it’s still a powerful app. Er, of course, the paid version is still the best. Yes, it’s better than Google Docs/Slide/Sheet. 

<div class="pb-app-box" data-theme="discover" data-lang="en"><a href="http://playboard.me/android/apps/com.mobisystems.office">OfficeSuite 8 + PDF Converter  (Playboard)</a> | <a href="https://play.google.com/store/apps/details?id=com.mobisystems.office&hl=en" rel="nofollow" target="_blank">OfficeSuite 8 + PDF Converter (Play Store)</a></div>
<script type="text/javascript" src="//playboard.me/widgets/pb-app-box/1/pb_load_app_box.js"></script>

<div class="pb-app-box" data-theme="discover" data-lang="en"><a href="http://playboard.me/android/apps/com.mobisystems.editor.office_registered">OfficeSuite 8 Pro + PDF  (Playboard)</a> | <a href="https://play.google.com/store/apps/details?id=com.mobisystems.editor.office_registered&hl=en" rel="nofollow" target="_blank">OfficeSuite 8 Pro + PDF (Play Store)</a><br></div>
<script type="text/javascript" src="//playboard.me/widgets/pb-app-box/1/pb_load_app_box.js"></script><br>


#Heads-up notifications#

You need this app if you are not running Android 5.0 (Lollipop users, you got this feature already, shh, don’t show off/hao-lian).

Google introduced the new Notification design for Android 5.0, Lollipop. It’s nice and make your notification more visible, accessible, and configurable.

Unfortunately, that’s only exclusive for Lollipop. Thanks to Heads-up notifications, those STILL using Android 4.4 and below will also have the ability to enjoy this beautiful pop-up (heads-up) notification. 

This app is highly customizable so you can choose which app to show in heads-up notification or when the notification will not show.

Note: Heads-up Notification is open source and available in [GitHub](https://github.com/SimenCodes/heads-up/)

<div class="pb-app-box" data-theme="discover" data-lang="en"><a href="http://playboard.me/android/apps/codes.simen.l50notifications">Heads-up notifications  (Playboard)</a> | <a href="https://play.google.com/store/apps/details?id=codes.simen.l50notifications&hl=en" rel="nofollow" target="_blank">Heads-up notifications (Play Store)</a></div>
<script type="text/javascript" src="//playboard.me/widgets/pb-app-box/1/pb_load_app_box.js"></script><br>


Okay, three apps should enough for today. I will talk some other apps next time (Part 2 is coming soon, haha). Anyway, remember to [SUBSCRIBE](http://eepurl.com/bdrwRz) to my newsletter in order to get the latest alert/notification when new post in updated. 

Any questions/comments? You are welcome to comment at the Discussion area below. 

Bye bye and have a nice day. 
* Chinese New Year is coming ^^

