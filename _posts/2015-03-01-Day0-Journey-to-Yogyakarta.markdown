---
layout: post
title:  "Journey to Yogyakarta - Day 0"
date:   2015-03-01 12:00:00
---

It's time to write something again, as I didn't write anything for nearly one month. Now it's time to write something interesting.<br><br>

#Day 0, The story before the journey

Hello Borobudur,<br>
Hello Volcano,<br>
Hello Yogyakarta,<br>
Hello Indonesia,<br>
Hello World. <br>

Last week, I travelled to Yogyakarta, Indonesia. 

Yogyakarta is also known as Jogja. The journey from Singapore /Johor Bahru to Yogyakarta by flight takes around 2 hours. Air Asia offers direct flight from Johor Bahru (JHB) to Yogyakarta (JOG).

Anyway, it is Yogyakarta, not Jakarta. So, is there anything special in Yogyakarta? Yes, Borobudur, Gunung Merapi (Volcano!) are located around Yogyakarta. And you also can see that volcano just from the Yogyakarta city! It sounds awesome right?  <br><br>

#Why Yogyakarta?
The reason I went to Yogyakarta was because I got a free ticket from Airasia (AirAsiaGo.com actually, which gives away a free flight ticket when you book a hotel through its website). It (THE ACCOMMODATION OR ACCOMMODATION + FLIGHT TICKETS?) costs around 800 Ringgit Malaysia for 3 people with a Triple/Double Room for 6 nights. Yes, it’s very very cheap so it’s time to fly again.<br><br>

#Landing failed?!
What? Yes, they (THEY - PLURAL, SHOULD BE PILOTS IN THIS CASE) (pilot, not me) failed to land the Airbus 320-200 on Yogyakarta Airport. 
<br>So? What happened next? I will talk about it on the next post and it’s coming soon :)

