---
layout: post
title:  "The Story of this Website #2"
date:   2015-04-02 12:00:00
---

Hello, you are now reading the second episode for “The Story of this Website”.

So, you might notice it, this blog completely has a brand new design. And it’s now powered by Polymer, which is new web development based on custom web components and it’s currently developing by Google.

#Polymer
It’s beautiful and responsive support. Now the web page will fit the browser automatically, even in mobile and tablet. Of course, this blog still a Jekyll blog.

BUT if are NOT using Google Chrome, you might face some weird problems while browsing this blog. Don’t worry, this is the disadvantage of Polymer. Since Polymer is still beta, it’s normal. The easiest way is to install Google Chrome and uninstall Internet Explorer. Er, why you still using IE ye?

#HTTPS
Security, security, security and security. We want a safe browsing experience for sure. So, I decided to ‘upgrade’ my blog from the default HTTP to the more secure HTTP, which is HTTPS (HTTP Secure) Obviously, it’s more secure than HTTP, that’s mean no more data leaks when you are browsing this blog (and homepage too) in public WiFi.

FOR DEVELOPER: Maybe you might ask me, is that free to get this SSL? The answer is YES! I get this SSL for completely free, no even single cent. How? I get this from Cloudflare. Again, for FREE. More info, you are welcome to visit Cloudflare website: [Cloudflare.com](https://www.cloudflare.com/ssl).

#New Feature: Search
Search. Now you can search the content in this blog. The search is accessible from the drawer panel which is at the left-hand site of the website or through this URL: [blog.limhenry.me/search](blog.limhenry.me/search)

#Add to Home Screen
What is this? If you are using Chrome for Android (I’m not sure about iOS), you can make this blog become an app. Just open this blog in Google Chrome (Android), and click option, select “Add to Homescreen”. Now, you have created an app for my blog. Ha! Congrats :)

#Subscribe
Lastly, remember to subscribe to my blog's newsletter through this link: [Subscribe Here](http://eepurl.com/bdrwRz). 

Thank you for support me and remember to disable or pause AdBlock while browsing my blog. Thank you and have a nice day.
